#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....         i3-Window-Manager-Installation                       ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.    Erst die Variablen anpassen!                        ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX    NICHT als root-user anmelden und ausführen!         ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN                                                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

##################
##  VARIABLE    ##
##################

country=Germany     # Hier dein Land für Reflector eintragen. Auf englisch: Germany, Switzerland, Austria,...
aur_helper=paru     # Welchen AUR-Helper? Es gibt: paru, yay, pacaur, pikaur, trizen

##########################
##  INSTALLATIONSSCRIPT ##
##########################

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c $country -a 12 --sort rate --save /etc/pacman.d/mirrorlist

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

git clone https://aur.archlinux.org/$aur_helper-bin.git
cd $aur_helper-bin/
makepkg -si --noconfirm

#$aur_helper -S --noconfirm system76-power
#sudo systemctl enable --now system76-power
#sudo system76-power graphics integrated
#$aur_helper -S --noconfirm gnome-shell-extension-system76-power-git
#$aur_helper -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

sudo pacman -S wayland sway terminator thunar firefox archlinux-wallpaper swaybg swayidle swayimg swaylock dmenu xorg-xwayland   

# sudo flatpak install -y spotify
# sudo flatpak install -y kdenlive

paru -S ly-git

sudo systemctl enable ly.service

mkdir -p ~/.config/sway
cp ~/arch-basic/sway/config ~/.config/sway/

/bin/echo -e "\e[1;32mNEUSTART IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
