#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....         Basis-Arch-Installationsscript                      ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.    für UEFI-Systeme                                    ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX                                                        ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN    Erst die Variablen anpassen!                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

##################
##  VARIABLE    ##
##################

efi_dir=/boot       # Ändere das Verzeichnis in /boot/efi, wenn du die EFI-Partition unter /boot/efi eingebunden hast
root_pass=P4ssw0rd  # Hier bei P4ssw0rd dein root-Passwort eintragen.
user_name=typ       # Hier dein Benutzername eintragen.
user_pass=P4ssw0rd  # Hier dein Benutzerpasswort.
hostname=archbox    # Hier deinen gewünschten Hostnamen setzen.

##########################
##  INSTALLATIONSSCRIPT ##
##########################

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
echo "de_DE.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" >> /etc/locale.conf
echo "KEYMAP=de-latin1" >> /etc/vconsole.conf
echo $hostname >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
echo root:$root_pass | chpasswd

# Du kannst xorg zu den Installationspaketen hinzufügen, ich füge es normalerweise beim DE- oder WM-Installationsskript hinzu.
# Du kannst das tlp-Paket entfernen, wenn du auf einem Desktop-PC oder VM installierst

pacman -S grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync reflector acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=$efi_dir --bootloader-id=GRUB 

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp # Du kannst diesen Befehl auskommentieren, wenn du tlp nicht installiert hast, siehe oben
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m $user_name
echo $user_name:$user_pass | chpasswd
usermod -aG libvirt $user_name

echo "$user_name ALL=(ALL) ALL" >> /etc/sudoers.d/$user_name


printf "\e[1;32mFertig! Tippe 'exit', 'umount -a' und dann 'reboot'.\e[0m"




